import java.util.Date;

public class DeliveryTry {
    private Date sent;
    private Integer msecondsFromDelivery;
    private Integer tries;

    public Integer getTries() {
        return tries;
    }

    public void setTries(Integer tries) {
        this.tries = tries;
    }

    DeliveryTry(){
        this.sent = new Date();
        this.msecondsFromDelivery = 0;
        this.tries = 0;
    }

    public Date getSent() {
        return sent;
    }

    public void setSent(Date sent) {
        this.sent = sent;
    }

    public Integer getMsecondsFromDelivery() {
        return msecondsFromDelivery;
    }

    public void setMsecondsFromDelivery(Integer msecondsFromDelivery) {
        this.msecondsFromDelivery = msecondsFromDelivery;
    }
}
