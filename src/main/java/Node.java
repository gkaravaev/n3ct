import messages.Message;
import messages.MessageInputStream;
import messages.MessageOutputStream;
import messages.MessageType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.*;

public class Node {

    private static final boolean NODE_ONLINE = true;
    private static final int MESSAGE_TIMEOUT = 5;
    private static final int MESSAGE_MSECONDS_GAP = 5000;
    private String name;
    private Integer lossPercent;
    private Integer port;
    private MessageOutputStream sendSocket;
    private MessageInputStream readSocket;
    private BufferedReader stdin;
    private HashMap<Message, DeliveryTry> notDelivered;
    private HashMap<InetSocketAddress, InetSocketAddress> neighbours;
    private InetSocketAddress reconnectNode;

    Node(String name, Integer lossPercent, Integer port) {
        try {
            this.name = name;
            this.port = port;
            this.lossPercent = lossPercent;
            this.readSocket = new MessageInputStream(new DatagramSocket(port));

            this.sendSocket = new MessageOutputStream(new DatagramSocket(port + 1));
            this.stdin = new BufferedReader(new InputStreamReader(System.in));
            this.notDelivered = new HashMap<>();
            this.neighbours = new HashMap<>();
            this.reconnectNode = null;
        } catch (SocketException e) {
            System.out.println("Socket initialization error!");
            System.exit(0);
        }
    }

    Node(String name, Integer lossPercent, Integer port, String ipAddr, Integer mPort) {
        try {
            this.name = name;
            this.port = port;
            this.lossPercent = lossPercent;
            this.readSocket = new MessageInputStream(new DatagramSocket(port));
            this.sendSocket = new MessageOutputStream(new DatagramSocket());
            this.stdin = new BufferedReader(new InputStreamReader(System.in));
            this.notDelivered = new HashMap<>();
            this.neighbours = new HashMap<>();
            neighbours.put(new InetSocketAddress(InetAddress.getByName(ipAddr), mPort), null);
            this.reconnectNode = new InetSocketAddress(InetAddress.getByName(ipAddr), mPort);

        } catch (SocketException e) {
            System.out.println("Socket initialization error!");
            System.exit(0);
        } catch (UnknownHostException e) {
            System.out.println("Host resolving error!");
            System.exit(0);
        }
    }


    private Message findMessageByUUID(UUID uuid) {
        for (Map.Entry<Message, DeliveryTry> msg : notDelivered.entrySet()) {
            if (msg.getKey().getUuid().equals(uuid)) return msg.getKey();
        }
        return null;
    }

    private Boolean isMessageLost() {
        return (new Random().nextDouble()) * 100 < this.lossPercent;
    }

    /**
     * Подсоединяемся к узлу reconnectNode,
     * добавляем его в список соседей.
     */

    public void connect() throws IOException {
        // коннектимся мы к реконнект узлу, так что если его нет, то заходить сюда смысла тоже нет
        // зайти сюда с таким условием мы можем только из мэйна
        if (this.reconnectNode == null)
            return;
        readSocket.enableTimeout(1000);
        // мы подключаемся к узлу который будет нашим реконнект узлом так что ему от нас ниче не надо
        // кроме порта на который сообщения посылать
        StringBuilder reconnectInfo = new StringBuilder(port.toString());
        // ну тут в целом всё ясно
        for (int i = 0; i < MESSAGE_TIMEOUT; i++) {
            System.out.println("Trying to connect to first host...");
            Message registration = new Message(reconnectInfo.toString(),
                    MessageType.NewConnection,
                    this.reconnectNode.getAddress(),
                    this.reconnectNode.getPort());
            sendSocket.sendMessage(registration);
            Message delivered = readSocket.readMessage();
            if (delivered != null && delivered.getMessageType().equals(MessageType.Delivered)) {
                // если у того, к кому мы подключились, есть узел для реконнекта, запоминаем его
                if (delivered.getMessage().split(" ").length == 2) {
                    String[] nodeInfo = delivered.getMessage().split(" ");
                    if (nodeInfo.length > 1) {
                        neighbours.put(reconnectNode,
                                new InetSocketAddress(InetAddress.getByName(nodeInfo[0]), Integer.parseInt(nodeInfo[1])));
                    } else {
                        neighbours.put(reconnectNode, null);
                    }
                } else {
                    neighbours.put(reconnectNode, null);
                }
                System.out.println("Connection successful!");
                return;
            }
        }
        System.out.println("Host timeout!");
        this.neighbours.remove(reconnectNode);
        reconnectNode = null;
    }

    /**
     * Метод для рассылки сообщения message всем соседям.
     * Любое сообщение пересылается с портом в начале, а то непонятно куда отвечать.
     * Я об этом забыл и два часа чинил.....
     *
     * @param notSendTo - адрес, которому сообщение не пересылается, обычно это тот, от кого сообщение пришло
     **/

    private void sendToNeighbours(String message, InetSocketAddress notSendTo, MessageType messageType) throws IOException {
        message = new StringBuilder().append(port).append(" ").append(message).toString();
        for (InetSocketAddress neighb : neighbours.keySet()) {
            if (!neighb.equals(notSendTo)) {
                Message toSend = new Message(message, messageType, neighb.getAddress(), neighb.getPort());
                sendSocket.sendMessage(toSend);
                notDelivered.put(toSend, new DeliveryTry());
            }
        }
        System.out.println("Message sent!");
    }


    private void readMessage(Message msg) throws IOException {
        switch (msg.getMessageType()) {
            case Delivered:
                // пришло сообщение о подтверждении доставки
                if (findMessageByUUID(UUID.fromString(msg.getMessage())) != null) {
                    notDelivered.remove(findMessageByUUID(UUID.fromString(msg.getMessage())));
                }
                break;
            case Message:
                // пришло сообщение, пересылаем соседям, шлем подтверждение
                int portFrom = Integer.parseInt(msg.getMessage().split(" ", 2)[0]);
                String message = msg.getMessage().split(" ", 2)[1];
                System.out.println(msg.getIpFrom() + ": " + message);
                sendToNeighbours(message, new InetSocketAddress(msg.getIpFrom(), portFrom), MessageType.Message);
                Message deliveryConfirmation = new Message(msg.getUuid().toString(),
                        MessageType.Delivered,
                        msg.getIpFrom(),
                        portFrom);
                sendSocket.sendMessage(deliveryConfirmation);
                break;
            case NodeUpdate:
                // у соседа сменился реконнект узел и он держит в курсе, поменяем инфу о нем
                String[] updateInfo = msg.getMessage().split(" ");
                if (updateInfo.length == 1) {
                    neighbours.put(new InetSocketAddress(msg.getIpFrom(), Integer.parseInt(updateInfo[0])), null);
                } else {
                    neighbours.put(new InetSocketAddress(msg.getIpFrom(), Integer.parseInt(updateInfo[0])),
                            new InetSocketAddress(InetAddress.getByName(updateInfo[1]), Integer.parseInt(updateInfo[2])));
                }
                Message deliveryConfirm = new Message(msg.getUuid().toString(),
                        MessageType.Delivered,
                        msg.getIpFrom(),
                        Integer.parseInt(updateInfo[0]));
                sendSocket.sendMessage(deliveryConfirm);
                System.out.println("Node updated his reconnect info!");
                break;
            case NewConnection:
                // к нам подсоединился новый узел, запоминаем его
                System.out.println("New neighbour connected!");
                String registerInfo = msg.getMessage();
                InetSocketAddress newNeighbour = new InetSocketAddress(msg.getIpFrom(), Integer.parseInt(registerInfo));
                neighbours.put(newNeighbour, null);
                System.out.println("Sending confirmation to " + newNeighbour.toString());
                // говорим куда реконнектиться в случае нашей смерти
                StringBuilder reconnectInfo = new StringBuilder();
                if (reconnectNode != null)
                    reconnectInfo.append(reconnectNode.getAddress().toString().replaceFirst("/", "")).append(" ").append(reconnectNode.getPort());
                sendSocket.sendMessage(new Message(reconnectInfo.toString(), MessageType.Delivered, msg.getIpFrom(), Integer.parseInt(msg.getMessage())));
                // если у нас нет реконнект нода, то мы можем его получить! объявляем первый узел
                // нашим реконнект нодом и держим соседей в курсе
                if (reconnectNode == null && neighbours.size() > 1) {
                    reconnectNode = new ArrayList<>(neighbours.keySet()).get(0);
                    String newRecInfo = reconnectNode.getAddress().toString().replaceFirst("/", "") + " " + reconnectNode.getPort();
                    sendToNeighbours(newRecInfo, reconnectNode, MessageType.NodeUpdate);
                }
                break;
        }
    }

    private void checkForTimeout() throws IOException {
        ArrayList<Message> toRemove = new ArrayList<>();
        for (Map.Entry<Message, DeliveryTry> ndm : notDelivered.entrySet()) {
            DeliveryTry deliveryCheck = ndm.getValue();
            deliveryCheck.setMsecondsFromDelivery((int) (new Date().getTime() - deliveryCheck.getSent().getTime()));
            // если спустя 5 попыток не отправилось, удаляем узел
            if (deliveryCheck.getMsecondsFromDelivery() > MESSAGE_MSECONDS_GAP * MESSAGE_TIMEOUT) {
                // запоминаем узел который удаляем
                InetSocketAddress toDelete = new InetSocketAddress(ndm.getKey().getIpFrom(), ndm.getKey().getPortFrom());
                // запоминаем куда реконнектиться
                InetSocketAddress reconnectLeftover = neighbours.remove(toDelete);
                // добавляем сообщение выбившее таймаут в список на удаление
                toRemove.add(ndm.getKey());
                // добавляем в список на удаление все сообщения, не доставленные этому узлу
                for (Map.Entry<Message, DeliveryTry> ndm2 : notDelivered.entrySet()) {
                    if (ndm.getKey().getIpFrom().equals(ndm2.getKey().getIpFrom())
                            && ndm.getKey().getPortFrom().equals(ndm2.getKey().getPortFrom())
                            && !ndm.getKey().getUuid().equals(ndm2.getKey().getUuid())) {
                        toRemove.add(ndm2.getKey());
                    }
                }
                System.out.println("Node timeouted!");

                // если это был наш узел на реконнект, надо обновить информацию об этом своим соседям
                if (toDelete.equals(reconnectNode)) {
                    String registerInfo = "";
                    // если у нас вообще есть соседи, и их больше одного (чтобы не вышло так,
                    // что сосед реконнектится сам к себе), то меняем узел на реконнект
                    if (neighbours != null && neighbours.size() > 1) {
                        reconnectNode = new ArrayList<>(neighbours.keySet()).get(0);
                        registerInfo = reconnectNode.getAddress().toString().replaceFirst("/", "") + " " + reconnectNode.getPort();
                    } else { // иначе у нас больше нет такого узла
                        reconnectNode = null;
                    }
                    // коннектимся к узлу, если это совсем новый для нас узел
                    if (neighbours != null && neighbours.get(reconnectNode) == null) {
                        connect();
                    }
                    // рассылаем соседям новую инфу
                    sendToNeighbours(registerInfo, reconnectNode, MessageType.NodeUpdate);
                }
                // каждые 5 секунд отправляем заново
            } else if ((deliveryCheck.getTries() + 1) * MESSAGE_MSECONDS_GAP < deliveryCheck.getMsecondsFromDelivery()) {
                deliveryCheck.setTries(deliveryCheck.getTries() + 1);
                sendSocket.sendMessage(ndm.getKey());
                System.out.println("Trying to deliver message once again: try " + deliveryCheck.getTries() + ", ms: " + deliveryCheck.getMsecondsFromDelivery());
            }
        }
        for (Message toR : toRemove) {
            notDelivered.remove(toR);
        }
    }

    public void start() {
        System.out.println("Node is online");
        readSocket.enableTimeout(100);
        while (NODE_ONLINE) {
            try {
                Message msg = readSocket.readMessage();
                if (msg != null) {
                    System.out.println("Got message from " + msg.getIpFrom().toString() + " " + msg.getPortFrom().toString());
                    if (!isMessageLost()) {
                        readMessage(msg);
                    } else {
                        System.out.println("Message lost because of loss percent");
                    }
                } else {
                    if (stdin.ready()) {
                        String newMessage = stdin.readLine();
                        sendToNeighbours(newMessage, null, MessageType.Message);
                    }
                }
                checkForTimeout();
            } catch (IOException e) {
                e.printStackTrace();
                this.readSocket.closeSocket();
                this.sendSocket.closeSocket();
                break;
            }
        }
    }


}
