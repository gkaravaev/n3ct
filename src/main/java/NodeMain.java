import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;

public class NodeMain {
    public static void main(String[] args) {
        Node node = null;
        if (args.length == 5) {
            try {
                node = new Node(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), args[3], Integer.parseInt(args[4]));
            } catch (SocketException | UnknownHostException e) {
                e.printStackTrace();
            }
        } else if (args.length == 3) {
            try {
                node = new Node(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]));
            } catch (SocketException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("3 or 5 args!");
            System.exit(0);
        }
        try {
            node.connect();
            node.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
