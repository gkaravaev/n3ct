package messages;

import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class MessageOutputStream {
    private DatagramSocket datagramSocket;
    public MessageOutputStream(DatagramSocket datagramSocket) {
        this.datagramSocket = datagramSocket;
    }

    public void closeSocket(){
        this.datagramSocket.close();
    }

    public void sendMessage(Message message) throws IOException {
        byte[] bUuid = message.getUuid().toString().replaceAll("-", "").getBytes();
        Integer messageType = message.getMessageType().ordinal();
        byte mt = messageType.byteValue();
        byte[] msg = message.getMessage().getBytes();
        DatagramPacket packet = new DatagramPacket(ArrayUtils.addAll(ArrayUtils.add(bUuid, mt), msg),
                msg.length + bUuid.length + 1,
                message.getIpFrom(),
                message.getPortFrom());
        datagramSocket.send(packet);

    }

}
