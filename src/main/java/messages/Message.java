package messages;

import java.net.InetAddress;
import java.util.UUID;

public class Message {
    private UUID uuid;
    private String message;
    private MessageType messageType;
    private InetAddress ipFrom;
    private Integer portFrom;

    public Message(String message, MessageType messageType, InetAddress ipFrom, Integer portFrom){
        this.uuid = UUID.randomUUID();
        this.message = message;
        this.messageType = messageType;
        this.ipFrom = ipFrom;
        this.portFrom = portFrom;
    }

    public Message(String uuid, String message, Integer messageType, InetAddress ipFrom, Integer portFrom) {
        this.uuid = UUID.fromString(new StringBuilder()
                .append(uuid, 0, 8)
                .append('-')
                .append(uuid, 8, 12)
                .append('-')
                .append(uuid, 12, 16)
                .append('-')
                .append(uuid, 16, 20)
                .append('-')
                .append(uuid, 20, 32).toString());
        this.message = message;
        switch (messageType) {
            case 0:
                this.messageType = MessageType.Delivered;
                break;
            case 1:
                this.messageType = MessageType.Message;
                break;
            case 2:
                this.messageType = MessageType.NewConnection;
                break;
            case 3:
                this.messageType = MessageType.NodeUpdate;
                break;
        }
        this.ipFrom = ipFrom;
        this.portFrom = portFrom;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getMessage() {
        return message;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public InetAddress getIpFrom() {
        return ipFrom;
    }

    public Integer getPortFrom() {
        return portFrom;
    }

    public void setIpFrom(InetAddress ipFrom) {
        this.ipFrom = ipFrom;
    }

    public void setPortFrom(Integer portFrom) {
        this.portFrom = portFrom;
    }
}
