package messages;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Arrays;

public class MessageInputStream {
    private DatagramSocket socket;
    public MessageInputStream(DatagramSocket socket){
        this.socket = socket;
    }

    public void closeSocket(){
        this.socket.close();
    }

    public void enableTimeout(int ms){
        try {
            socket.setSoTimeout(ms);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public Message readMessage() throws IOException {
        DatagramPacket msgPacket = new DatagramPacket(new byte[65571], 65571);
        try {
            socket.receive(msgPacket);
        } catch (SocketTimeoutException e) {
            return null;
        }
        byte[] uuid = Arrays.copyOfRange(msgPacket.getData(), 0, 32);
        byte[] msgType = Arrays.copyOfRange(msgPacket.getData(), 32, 33);
        byte[] msg = Arrays.copyOfRange(msgPacket.getData(), 33, msgPacket.getData().length);
        return new Message(new String(uuid).trim(), new String(msg).trim(), new BigInteger(msgType).intValue(), msgPacket.getAddress(), msgPacket.getPort());

    }

    public DatagramSocket getSocket() {
        return socket;
    }
}
