package messages;

public enum MessageType {
    Delivered, Message, NewConnection, NodeUpdate
}
